Feature: Withdraw money from bank account

  Scenario: Withdraw money
    Given I have $100 in my bank account
    When I withdraw $20
    Then I should have $80 in my bank account


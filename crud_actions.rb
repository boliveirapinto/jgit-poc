#!/usr/bin/env jruby

require "rjgit"
include RJGit

# create reference to the git repo
repo = Repo.new(".")

puts "Getting commits from master"
it = 0

repo.commits("master").each do |commit|
  it = it+1
  puts "#{it}. #{commit.message}"
end

puts "\n** Create a new file **"
puts "What is the file name?"
filename = gets.chomp

out_file = File.open(filename, "w")
out_file.close

puts "Created the file"
puts "Commiting the file to git"
puts "What is the commit message?"
add_commit_message = gets.chomp

puts "Commiting..."
repo.add(filename)
repo.commit(add_commit_message)

puts "Pushing the changes to the remote repo"
repo.git.push("origin", ["master"])
puts "Done"

puts "\n** Edit a file **"
puts "Editing '#{filename}'"
puts "Write some content for the file:"
file_content = gets

puts "Editing the file"
out_file = File.open(filename, "w")
out_file.puts file_content
out_file.close

puts "Comitting the changes"
puts "What is the commit message?"
edit_commit_message = gets.chomp

puts "Commiting..."
repo.add(filename)
repo.commit(edit_commit_message)

puts "Pushing the changes to the remote repo"
repo.git.push("origin", ["master"])
puts "Done"

puts "\n** Deleting a file **"
puts "Deleting '#{filename}'"

File.delete(filename)

puts "Comitting the changes"
puts "What is the commit message?"
delete_commit_message = gets.chomp

puts "Commiting..."
repo.remove(filename)
repo.commit(delete_commit_message)

puts "Pushing the changes to the remote repo"
repo.git.push("origin", ["master"])
puts "Done"

puts "\nGetting commits from master"
it = 0

repo.commits("master").each do |commit|
  it = it+1
  puts "#{it}. #{commit.message}"
end


